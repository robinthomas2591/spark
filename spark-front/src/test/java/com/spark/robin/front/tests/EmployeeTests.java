package com.spark.robin.front.tests;

import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.util.UriComponentsBuilder;

import com.spark.robin.controller.endpoints.EndPoints;
import com.spark.robin.front.tests.spec.Tests;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class EmployeeTests extends Tests {

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void getEmployeeTest() throws Exception {
        Map<String, String> urlVariables = new HashMap<>();
        urlVariables.put("id", EMPLOYEE_ID);

        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(EndPoints.GET_EMPLOYEE);

        ResponseEntity<String> responseEntity = this.restTemplate.exchange(
                builder.buildAndExpand(urlVariables).toUri(), HttpMethod.GET, null, String.class);
        assertTrue(responseEntity.getStatusCode() == HttpStatus.OK);
    }

    @Test
    public void createEmployeeTest() throws Exception {
        Map<String, String> urlVariables = new HashMap<>();
        urlVariables.put("id", EMPLOYEE_ID);

        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(EndPoints.CREATE_EMPLOYEE);

        ResponseEntity<String> responseEntity = this.restTemplate.exchange(
                builder.buildAndExpand(urlVariables).toUri(), HttpMethod.POST, null, String.class);
        assertTrue(responseEntity.getStatusCode() == HttpStatus.OK);
    }

    @Test
    public void updateEmployeeTest() throws Exception {
        Map<String, String> urlVariables = new HashMap<>();
        urlVariables.put("id", EMPLOYEE_ID);

        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(EndPoints.UPDATE_EMPLOYEE);

        ResponseEntity<String> responseEntity = this.restTemplate.exchange(
                builder.buildAndExpand(urlVariables).toUri(), HttpMethod.PUT, null, String.class);
        assertTrue(responseEntity.getStatusCode() == HttpStatus.OK);
    }

    @Test
    public void deleteEmployeeTest() throws Exception {
        Map<String, String> urlVariables = new HashMap<>();
        urlVariables.put("id", EMPLOYEE_ID);

        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(EndPoints.DELETE_EMPLOYEE);

        ResponseEntity<String> responseEntity = this.restTemplate.exchange(
                builder.buildAndExpand(urlVariables).toUri(), HttpMethod.DELETE, null, String.class);
        assertTrue(responseEntity.getStatusCode() == HttpStatus.OK);
    }

    @Test
    public void getEmployeeTreeTest() throws Exception {
        Map<String, String> urlVariables = new HashMap<>();
        urlVariables.put("id", EMPLOYEE_ID);

        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(EndPoints.GET_EMPLOYEE_TREE);

        ResponseEntity<String> responseEntity = this.restTemplate.exchange(
                builder.buildAndExpand(urlVariables).toUri(), HttpMethod.GET, null, String.class);
        assertTrue(responseEntity.getStatusCode() == HttpStatus.OK);
    }
}