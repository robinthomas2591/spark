package com.spark.robin.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.spark.robin.controller.endpoints.EndPoints;
import com.spark.robin.dto.Employee;
import com.spark.robin.dto.Request;
import com.spark.robin.service.EmployeeService;
import com.spark.robin.service.validation.spec.ValidationErrors;

@RestController
public class EmployeeController {
    @Autowired
    private EmployeeService service;

    @GetMapping(value = EndPoints.GET_EMPLOYEE)
    public ResponseEntity<?> getEmployee(@PathVariable(value = "id") String id) {
        Employee employee = service.getEmployee(id);

        // Check if employee exists.
        if (employee == null) {
            Map<String, String> errors = new HashMap<>();
            errors.put("errors", ValidationErrors.EMPLOYEE_DOESNT_EXIST.toString());
            return new ResponseEntity<>(errors, HttpStatus.UNPROCESSABLE_ENTITY);
        }

        return new ResponseEntity<>(employee, HttpStatus.OK);
    }

    @GetMapping(value = EndPoints.GET_EMPLOYEES)
    public ResponseEntity<?> getEmployees(
            @RequestParam(value = "employees", required = true) List<String> employeeIdList) {
        List<Employee> employees = service.getEmployees(employeeIdList);
        return new ResponseEntity<>(employees, HttpStatus.OK);
    }

    @PostMapping(value = EndPoints.CREATE_EMPLOYEE)
    public ResponseEntity<?> createEmployee(@PathVariable(value = "id") String id,
            @RequestBody Request request) {
        Employee dto = Request.toEmployee(id, request);

        List<ValidationErrors> errorList = service.createEmployee(dto);

        // Error from DB.
        if (errorList.contains(ValidationErrors.DB_ERROR)) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        List<String> errorResponseList = ValidationErrors.toStringList(errorList);
        if (errorResponseList.size() > 0) {
            Map<String, List<String>> errors = new HashMap<>();
            errors.put("errors", errorResponseList);
            return new ResponseEntity<>(errors, HttpStatus.UNPROCESSABLE_ENTITY);
        }

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PutMapping(value = EndPoints.UPDATE_EMPLOYEE)
    public ResponseEntity<?> updateEmployee(@PathVariable(value = "id") String id,
            @RequestBody Request request) {
        Employee dto = Request.toEmployee(id, request);

        List<ValidationErrors> errorList = service.updateEmployee(dto);

        // Error from DB.
        if (errorList.contains(ValidationErrors.DB_ERROR)) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        List<String> errorResponseList = ValidationErrors.toStringList(errorList);
        if (errorResponseList.size() > 0) {
            Map<String, List<String>> errors = new HashMap<>();
            errors.put("errors", errorResponseList);
            return new ResponseEntity<>(errors, HttpStatus.UNPROCESSABLE_ENTITY);
        }

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @DeleteMapping(value = EndPoints.DELETE_EMPLOYEE)
    public ResponseEntity<?> deleteEmployee(@PathVariable(value = "id") String id) {
        List<ValidationErrors> errorList = service.deleteEmployee(id);

        // Error from DB.
        if (errorList.contains(ValidationErrors.DB_ERROR)) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        List<String> errorResponseList = ValidationErrors.toStringList(errorList);
        if (errorResponseList.size() > 0) {
            Map<String, List<String>> errors = new HashMap<>();
            errors.put("errors", errorResponseList);
            return new ResponseEntity<>(errors, HttpStatus.UNPROCESSABLE_ENTITY);
        }

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping(value = EndPoints.GET_EMPLOYEE_TREE)
    public ResponseEntity<?> getEmployeeTree(@PathVariable(value = "id") String id) {
        List<Employee> tree = service.getEmployeeTree(id);
        return new ResponseEntity<>(tree, HttpStatus.OK);
    }

}