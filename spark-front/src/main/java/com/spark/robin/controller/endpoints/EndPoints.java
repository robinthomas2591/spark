package com.spark.robin.controller.endpoints;

public class EndPoints {
    public static final String VERSION = "v1";
    public static final String ROOT = "/api/" + VERSION + "/";

    public static final String EMPLOYEE = ROOT + "employees/";
    public static final String GET_EMPLOYEE = EMPLOYEE + "{id}";
    public static final String CREATE_EMPLOYEE = EMPLOYEE + "{id}";
    public static final String UPDATE_EMPLOYEE = EMPLOYEE + "{id}";
    public static final String DELETE_EMPLOYEE = EMPLOYEE + "{id}";

    public static final String GET_EMPLOYEE_TREE = EMPLOYEE + "{id}/tree";
    public static final String GET_EMPLOYEES = EMPLOYEE;
}