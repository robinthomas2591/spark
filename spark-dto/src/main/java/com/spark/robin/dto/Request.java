package com.spark.robin.dto;

import java.time.LocalDate;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Builder;

import org.springframework.lang.Nullable;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.spark.robin.dto.Employee.EmployeeBuilder;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Request {
    @JsonProperty("start_date")
    private LocalDate startDate;

    @Nullable
    @JsonProperty("end_date")
    private LocalDate endDate;

    private String name;

    @Nullable
    @JsonProperty("boss_id")
    private String bossId;

    public static Employee toEmployee(String id, Request request) {
        if (id == null || request == null) {
            return null;
        }

        EmployeeBuilder builder = Employee.builder();
        builder.id(id);
        builder.startDate(request.getStartDate());
        builder.endDate(request.getEndDate());
        builder.name(request.getName());
        builder.bossId(request.getBossId());
        return builder.build();
    }

}