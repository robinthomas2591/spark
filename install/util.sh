#!/bin/bash

# Create the WAR.
build_war_file() {
  if [ "$REBUILD" == "-r" ] || [ ! -f "$WAR_PATH_" ]; then
    if [ ! -z "$(command -v mvn >/dev/null)" ]; then
      echo "[*] Maven not installed! Please install it"
      exit 1
    fi

    mvn clean install
  else
    echo "WAR already exists!"
    echo "If you need to create a new WAR, please run with \"-r\" option"
  fi
}


# Copy the WAR file to docker container.
copy_war_file() {
  local DOCKER_CONTAINER_NAME=$1

  echo "[*] Copying the WAR file to docker"
  docker cp ${WAR_PATH_} ${DOCKER_CONTAINER_NAME}:/usr/local/tomcat/webapps/${WAR_NEW_NAME_}
}
