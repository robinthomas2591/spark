# DESIGN

### Design Decisions

An employee system will normally store multiple records of an employee (past records, current records, and future records). For example, when an employee changes team or gets a promotion, rather than updating the existing record, we should create a new record with a different start date. A record should be allowed to be updated only for minor changes. Keeping track for these multiple records can help with creating more useful features for the customer. But considering the complexity of this assignment, such a system will be too hard to be created in a day. As such, I'm opting for a system which stores only one record of an employee.

Rather than accepting one employee at a time, the system can handle multiple employees at a time. This improves the I/O between the DB and the DAO layer. Pagination for such APIs is a good idea, but considering the complexity of that feature for this assignment, I'm choosing to ignore it.

All the APIs output JSON. HTTP status codes can be used along with it to understand any error if it occurs. Since its JSON, it's easier for the system to create requests to the API server and parse the output. The reason why I opted for an API server is because, its now a standalone module that can be built and deployed separately (as noted from the setup scripts) from the other systems. Any bug outside the API server will not affect the API server.

Unit tests in the Service module covers most of the code. The only code it doesnt cover are some DB specific code (which are actually mocked in other files). It also comes with integration tests in front module to test the API endpoints.
