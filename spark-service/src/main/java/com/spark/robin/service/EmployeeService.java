package com.spark.robin.service;

import java.util.List;

import com.spark.robin.dto.Employee;
import com.spark.robin.service.validation.spec.ValidationErrors;

public interface EmployeeService {
    public List<ValidationErrors> createEmployee(Employee dto);

    public List<ValidationErrors> updateEmployee(Employee dto);

    public List<ValidationErrors> deleteEmployee(String id);

    public Employee getEmployee(String id);

    public List<Employee> getEmployeeTree(String id);

    public List<Employee> getEmployees(List<String> employeeIdList);

    /**
     * Function to clear the internal data structures (for mockDAO only).
     */
    public void clear();

}