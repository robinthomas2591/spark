package com.spark.robin.service.dao;

import java.util.List;

import com.spark.robin.dto.Employee;

public interface EmployeeDao {
    /**
     * Function to create an employee or update an employee in the DB.
     *
     * @param dto
     */
    public void insert(Employee dto);

    /**
     * Function to delete an employee from the DB.
     *
     * @param dto
     */
    public void delete(Employee dto);

    /**
     * Function to retrieve an employee details from the DB.
     *
     * @param empId
     * @return Employee
     */
    public Employee get(String empId);

    /**
     * Function to retrieve a list of employees
     *
     * @param employeeIdList
     * @return
     */
    public List<Employee> get(List<String> employeeIdList);

    /**
     * Function (implemented in MockDao only) to clear the internal data structures of the DAO.
     */
    public void clear();
}