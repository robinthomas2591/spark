package com.spark.robin.service.impl;

import java.util.ArrayList;
import java.util.List;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.spark.robin.dto.Employee;
import com.spark.robin.service.EmployeeService;
import com.spark.robin.service.dao.EmployeeDao;
import com.spark.robin.service.validation.spec.ValidationErrors;

@Slf4j
@Service
public class EmployeeServiceImpl implements EmployeeService {

    @Autowired
    private EmployeeDao employeeDao;

    @Override
    public List<ValidationErrors> createEmployee(Employee dto) {
        List<ValidationErrors> errors = new ArrayList<>();

        // Check whether input is valid.
        if (dto == null || dto.getId() == null) {
            errors.add(ValidationErrors.EMPTY_EMPLOYEE_ID);
            return errors;
        }

        // Check whether employee already exists.
        Employee emp = employeeDao.get(dto.getId());
        if (emp != null) {
            errors.add(ValidationErrors.EMPLOYEE_EXIST);
            return errors;
        }

        // Write to DB.
        try {
            employeeDao.insert(dto);
        } catch (Exception e) {
            log.error("exception occured with creating employee in repository");
            log.error("Error:", e);
            errors.add(ValidationErrors.DB_ERROR);
            return errors;
        }

        return errors;
    }

    @Override
    public List<ValidationErrors> updateEmployee(Employee dto) {
        List<ValidationErrors> errors = new ArrayList<>();

        // Check whether input is valid.
        if (dto == null || dto.getId() == null) {
            errors.add(ValidationErrors.EMPTY_EMPLOYEE_ID);
            return errors;
        }

        // Check whether employee does exist.
        Employee emp = employeeDao.get(dto.getId());
        if (emp == null) {
            errors.add(ValidationErrors.EMPLOYEE_DOESNT_EXIST);
            return errors;
        }

        try {
            employeeDao.insert(dto);
        } catch (Exception e) {
            log.error("exception occured with updating employee in repository");
            log.error("Error:", e);
            errors.add(ValidationErrors.DB_ERROR);
            return errors;
        }

        return errors;
    }

    @Override
    public List<ValidationErrors> deleteEmployee(String id) {
        List<ValidationErrors> errors = new ArrayList<>();

        // Check whether input is valid.
        if (id == null || id == "") {
            errors.add(ValidationErrors.EMPTY_EMPLOYEE_ID);
            return errors;
        }

        // Check whether employee does exist.
        Employee emp = employeeDao.get(id);
        if (emp == null) {
            errors.add(ValidationErrors.EMPLOYEE_DOESNT_EXIST);
            return errors;
        }

        try {
            employeeDao.delete(emp);
        } catch (Exception e) {
            log.error("exception occured with deleting employee in repository");
            log.error("Error:", e);
            errors.add(ValidationErrors.DB_ERROR);
            return errors;
        }

        return errors;
    }

    @Override
    public Employee getEmployee(String id) {
        // Check whether input is valid.
        if (id == null || id == "") {
            log.info("id to getEmployee is null");
            return null;
        }

        return employeeDao.get(id);
    }

    @Override
    public List<Employee> getEmployeeTree(String id) {
        // Check whether input is valid.
        if (id == null) {
            log.info("dto object to getEmployeeTree is null");
            return null;
        }

        List<Employee> employees = new ArrayList<>();

        String employeeId = id;
        while (true) {
            Employee emp = employeeDao.get(employeeId);
            if (emp == null) {
                break;
            }

            employees.add(emp);
            if (emp.getBossId() == null) {
                break;
            }

            employeeId = emp.getBossId();
        }

        return employees;
    }

    @Override
    public List<Employee> getEmployees(List<String> employeeIdList) {
        // Check whether input is valid.
        if (CollectionUtils.isEmpty(employeeIdList)) {
            log.info("idList to getEmployees is empty");
            return new ArrayList<>();
        }

        return employeeDao.get(employeeIdList);
    }

    @Override
    public void clear() {
        employeeDao.clear();
    }
}