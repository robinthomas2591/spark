package com.spark.robin.service.dao.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.spark.robin.dto.Employee;
import com.spark.robin.service.dao.EmployeeDao;
import com.spark.robin.service.dao.EmployeeRepository;

@Service
public class EmployeeDaoImpl implements EmployeeDao {

    @Autowired
    private EmployeeRepository repository;

    @Override
    public void insert(Employee dto) {
        repository.save(dto);
    }

    @Override
    public void delete(Employee dto) {
        repository.delete(dto);
    }

    @Override
    public Employee get(String empId) {
        Optional<Employee> employee = repository.findById(empId);
        if (employee.isPresent()) {
            return employee.get();
        }

        return null;
    }

    @Override
    public List<Employee> get(List<String> employeeIdList) {
        return repository.findAllById(employeeIdList);
    }

    @Override
    public void clear() {
        // Just for unit tests only.
        // No need to clear the data from DB.
    }

}
