package com.spark.robin.service.validation.spec;

import java.util.ArrayList;
import java.util.List;

import org.springframework.util.CollectionUtils;

public enum ValidationErrors {
    NO_ERROR("NO_ERROR"),
    EMPTY_EMPLOYEE_ID("Employee Id is empty"),
    EMPLOYEE_DOESNT_EXIST("Given employee Id doesn't exist"),
    EMPLOYEE_EXIST("Given employee already exist"),
    DB_ERROR("DB error");

    private String errorCode;

    ValidationErrors(String errorCode) {
        this.errorCode = errorCode;
    }

    @Override
    public String toString() {
        return this.errorCode;
    }

    /**
     * Function to convert a list of ValidationErrors to a list of string.
     *
     * @param errorList
     *            - list of ValidationErrors
     * @return List<String>
     */
    public static List<String> toStringList(List<ValidationErrors> errorList) {
        List<String> errorResponseList = new ArrayList<>();

        if (!CollectionUtils.isEmpty(errorList)) {
            for (ValidationErrors error : errorList) {
                errorResponseList.add(error.toString());
            }
        }

        return errorResponseList;
    }

}