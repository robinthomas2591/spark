package com.spark.robin.service.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.spark.robin.dto.Employee;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, String> {

}
