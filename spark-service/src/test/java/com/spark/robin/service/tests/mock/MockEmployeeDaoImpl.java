package com.spark.robin.service.tests.mock;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.util.CollectionUtils;

import com.spark.robin.dto.Employee;
import com.spark.robin.service.dao.EmployeeDao;

public class MockEmployeeDaoImpl implements EmployeeDao {

    Map<String, Employee> map = new HashMap<>();

    @Override
    public void insert(Employee dto) {
        map.put(dto.getId(), dto);
    }

    @Override
    public void delete(Employee dto) {
        map.remove(dto.getId());
    }

    @Override
    public Employee get(String empId) {
        return map.get(empId);
    }

    @Override
    public List<Employee> get(List<String> employeeIdList) {
        List<Employee> employees = new ArrayList<>();

        if (!CollectionUtils.isEmpty(employeeIdList)) {
            for (String id : employeeIdList) {
                Employee emp = map.get(id);
                if (emp != null) {
                    employees.add(emp);
                }
            }
        }

        return employees;
    }

    @Override
    public void clear() {
        map.clear();
    }

}