package com.spark.robin.service.tests.service;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.spark.robin.dto.Employee;
import com.spark.robin.service.EmployeeService;
import com.spark.robin.service.tests.config.TestConfig;
import com.spark.robin.service.tests.service.spec.Tests;
import com.spark.robin.service.validation.spec.ValidationErrors;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { TestConfig.class })
public class ServiceTests extends Tests {
    @Autowired
    private EmployeeService employeeService;

    @Test
    public void smokeTest() {
        assertTrue(!Objects.isNull(employeeService));
    }

    @Test
    public void testEmptyRepo() {
        Employee emp = employeeService.getEmployee(EMPLOYEE_ID);
        assertTrue(emp == null);
        employeeService.clear();
    }

    @Test
    public void testInsert() {
        Employee emp = employeeService.getEmployee(EMPLOYEE_ID);
        assertTrue(emp == null);

        emp = Employee.builder().id(EMPLOYEE_ID).build();
        employeeService.createEmployee(emp);
        assertTrue(employeeService.getEmployee(EMPLOYEE_ID) != null);

        employeeService.clear();
    }

    @Test
    public void testInsertEmptyEmpID() {
        List<ValidationErrors> errors = employeeService.createEmployee(null);
        assertTrue(errors.contains(ValidationErrors.EMPTY_EMPLOYEE_ID));

        Employee emp = Employee.builder().id(EMPTY_EMPLOYEE_ID).build();
        errors = employeeService.createEmployee(emp);
        assertTrue(errors.contains(ValidationErrors.EMPTY_EMPLOYEE_ID));

        employeeService.clear();
    }

    @Test
    public void testInsertError() {
        Employee emp = employeeService.getEmployee(EMPLOYEE_ID);
        assertTrue(emp == null);

        emp = Employee.builder().id(EMPLOYEE_ID).build();
        employeeService.createEmployee(emp);
        List<ValidationErrors> errors = employeeService.createEmployee(emp);
        assertTrue(errors.contains(ValidationErrors.EMPLOYEE_EXIST));

        employeeService.clear();
    }

    @Test
    public void testUpdate() {
        Employee emp = employeeService.getEmployee(EMPLOYEE_ID);
        assertTrue(emp == null);

        emp = Employee.builder().id(EMPLOYEE_ID).build();
        employeeService.createEmployee(emp);
        assertTrue(employeeService.getEmployee(EMPLOYEE_ID) != null);

        List<ValidationErrors> errors = employeeService.updateEmployee(emp);
        assertTrue(errors.size() == 0);

        employeeService.clear();
    }

    @Test
    public void testUpdateEmptyEmpID() {
        List<ValidationErrors> errors = employeeService.updateEmployee(null);
        assertTrue(errors.contains(ValidationErrors.EMPTY_EMPLOYEE_ID));

        Employee emp = Employee.builder().id(EMPTY_EMPLOYEE_ID).build();
        errors = employeeService.updateEmployee(emp);
        assertTrue(errors.contains(ValidationErrors.EMPTY_EMPLOYEE_ID));

        employeeService.clear();
    }

    @Test
    public void testUpdateErrors() {
        Employee emp = employeeService.getEmployee(EMPLOYEE_ID);
        assertTrue(emp == null);

        emp = Employee.builder().id(EMPLOYEE_ID).build();
        List<ValidationErrors> errors = employeeService.updateEmployee(emp);
        assertTrue(errors.contains(ValidationErrors.EMPLOYEE_DOESNT_EXIST));

        employeeService.clear();
    }

    @Test
    public void testDelete() {
        Employee emp = employeeService.getEmployee(EMPLOYEE_ID);
        assertTrue(emp == null);

        emp = Employee.builder().id(EMPLOYEE_ID).build();
        employeeService.createEmployee(emp);
        assertTrue(employeeService.getEmployee(EMPLOYEE_ID) != null);

        employeeService.deleteEmployee(EMPLOYEE_ID);
        assertTrue(employeeService.getEmployee(EMPLOYEE_ID) == null);

        employeeService.clear();
    }

    @Test
    public void testDeleteEmptyEmpID() {
        List<ValidationErrors> errors = employeeService.deleteEmployee(null);
        assertTrue(errors.contains(ValidationErrors.EMPTY_EMPLOYEE_ID));

        errors = employeeService.deleteEmployee("");
        assertTrue(errors.contains(ValidationErrors.EMPTY_EMPLOYEE_ID));

        employeeService.clear();
    }

    @Test
    public void testDeleteError() {
        Employee emp = employeeService.getEmployee(EMPLOYEE_ID);
        assertTrue(emp == null);

        emp = Employee.builder().id(EMPLOYEE_ID).build();
        employeeService.createEmployee(emp);
        assertTrue(employeeService.getEmployee(EMPLOYEE_ID) != null);

        employeeService.deleteEmployee(EMPLOYEE_ID);
        List<ValidationErrors> errors = employeeService.deleteEmployee(EMPLOYEE_ID);
        assertTrue(errors.contains(ValidationErrors.EMPLOYEE_DOESNT_EXIST));

        employeeService.clear();
    }

    @Test
    public void testGetEmptyEmpId() {
        assertTrue(employeeService.getEmployee(EMPTY_EMPLOYEE_ID) == null);
        assertTrue(employeeService.getEmployee("") == null);
    }

    @Test
    public void testGetEmployeesEmptyEmpId() {
        List<String> empList = new ArrayList<>();
        List<Employee> result = employeeService.getEmployees(empList);
        assertTrue(result.size() == 0);

        empList = new ArrayList<String>(Arrays.asList("1"));
        result = employeeService.getEmployees(empList);
        assertTrue(result.size() == 0);
    }

    @Test
    public void testGetEmployeeTree() {
        Employee emp = employeeService.getEmployee(EMPLOYEE_ID);
        assertTrue(emp == null);

        Employee emp1 = Employee.builder().id(EMPLOYEE_ID).build();
        employeeService.createEmployee(emp1);
        Employee emp2 = Employee.builder().id(EMPLOYEE_ID_2).bossId(EMPLOYEE_ID).build();
        employeeService.createEmployee(emp2);
        Employee emp3 = Employee.builder().id(EMPLOYEE_ID_3).bossId(EMPLOYEE_ID_2).build();
        employeeService.createEmployee(emp3);

        List<Employee> employees = employeeService.getEmployeeTree(EMPLOYEE_ID_3);
        assertTrue(employees.get(0).getId() == EMPLOYEE_ID_3);
        assertTrue(employees.get(1).getId() == EMPLOYEE_ID_2);
        assertTrue(employees.get(2).getId() == EMPLOYEE_ID);

        employeeService.clear();
    }
}