package com.spark.robin.service.tests.service.spec;

public abstract class Tests {
    public static final String EMPLOYEE_ID = "1";
    public static final String EMPTY_EMPLOYEE_ID = null;

    public static final String EMPLOYEE_ID_2 = "2";
    public static final String EMPLOYEE_ID_3 = "3";
    public static final String EMPLOYEE_ID_4 = "4";
}