package com.spark.robin.service.tests.config;

import org.springframework.context.annotation.Bean;

import com.spark.robin.service.EmployeeService;
import com.spark.robin.service.dao.EmployeeDao;
import com.spark.robin.service.impl.EmployeeServiceImpl;
import com.spark.robin.service.tests.mock.MockEmployeeDaoImpl;

public class TestConfig {
    @Bean
    public EmployeeDao driverDao() {
        return new MockEmployeeDaoImpl();
    }

    @Bean
    public EmployeeService driverService() {
        return new EmployeeServiceImpl();
    }

}