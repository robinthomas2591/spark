package com.spark.robin.service.tests;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.spark.robin.service.tests.config.TestConfig;
import com.spark.robin.service.validation.spec.ValidationErrors;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { TestConfig.class })
public class ValidationTests {

    @Test
    public void testValidationErrorStrings() {
        assertNotNull(ValidationErrors.EMPLOYEE_DOESNT_EXIST.toString());
        assertNotNull(ValidationErrors.valueOf("NO_ERROR"));
    }

    @Test
    public void testValidationErrorListStrings() {
        List<ValidationErrors> errors = new ArrayList<>();
        errors.add(ValidationErrors.EMPLOYEE_DOESNT_EXIST);

        List<String> errorStringList = ValidationErrors.toStringList(errors);
        assertTrue(errorStringList.size() == 1);

        errorStringList = ValidationErrors.toStringList(new ArrayList<>());
        assertTrue(errorStringList.size() == 0);
    }

}