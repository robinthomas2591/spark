# API Assignment


### Tech stack overview

* Java
* Maven
* Spring Boot
* Docker, Docker Compose
* PostgreSQL
* Nginx
* Bash


### Infrastructure Requirements

* Ubuntu 16.04
* JDK 1.8 (if you need to modify any code and rebuild the WAR)
* Maven 3.3.1+ (if you need to modify any code and rebuild the WAR)
* Docker 1.13.1 (which is the version I had tested on)
* Docker Compose 1.8.0 (which is the version I had tested on)


### Build and Deploy

It also has an easy installation script, which can be run by running the below commands.

```sh
$ cd spark
$ chmod +x build.sh
$ ./build.sh
```
It will check to see whether the WAR file exists in the target directory. If not, it'll try to build the WAR file again.

If you want to rebuild the WAR (if you made any changes in the code), you can run the script like shown below.

```sh
$ cd spark
$ chmod +x build.sh
$ ./build.sh -r
```

Once the WAR is built, next step is to deploy.

Current setup is:

* Nginx server
* 1 app servers
* 1 postgresql server

The number of app servers can be increased to handle the demand. The postgresql server can also be scaled as well.
Nginx server runs as load balancer.


```sh
$ cd spark
$ chmod +x install.sh
$ sudo ./install.sh
```

The above script will create all necessary Docker images (if they do not exist), check to see whether Docker containers exist (if not, create them), and start them if they are not running. Then the WAR is copied over to Tomcat container.

You can access the API at: [http://localhost/spark/api/v1/employees](http://localhost/spark/api/v1/employees)

* GET EMPLOYEE - GET [http://localhost/spark/api/v1/employees/{id}](http://localhost/spark/api/v1/employees/{id})
* CREATE EMPLOYEE - POST [http://localhost/spark/api/v1/employees/{id}](http://localhost/spark/api/v1/employees/{id})
* UPDATE EMPLOYEE - PUT [http://localhost/spark/api/v1/employees/{id}](http://localhost/spark/api/v1/employees/{id})
* DELETE EMPLOYEE - DELETE [http://localhost/spark/api/v1/employees/{id}](http://localhost/spark/api/v1/employees/{id})
* GET EMPLOYEE TREE - [http://localhost/spark/api/v1/employees/{id}/tree](http://localhost/spark/api/v1/employees/{id}/tree)
